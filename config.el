;;; $DOOMDIR/config.el -*- lexical-binding: t; -*-

(setq-default custom-file null-device)
(setq display-line-numbers-type 'relative)

;;; $DOOMDIR/init.el -*- lexical-binding: t; -*-

(doom!
 :ui
 doom
 doom-dashboard
 (modeline +light)

 :editor
 (evil +everywhere)
 (format +apheleia)

 :completion
 orderless
 (selectrum +orderless +savehist)

 :tools
 (lsp +eglot)

 :lang
 asciidoc
 markdown
 typescript
 haskell
 (go +lsp)

 :config
 (default +bindings))

;;; lang/asciidoc/config.el -*- lexical-binding: t; -*-

(use-package! asciidoc)

(use-package! adoc-mode
  :commands adoc-mode)

;;; editor/evil/packages.el -*- lexical-binding: t; -*-

(package! evil)

(when (featurep! :editor evil +everywhere)
  (package! evil-collection))

;;; editor/evil/config.el -*- lexical-binding: t; -*-

(use-package! evil
  :init
  (when (featurep! :editor evil +everywhere)
    (setq evil-want-keybinding nil))
  :config
  (evil-mode 1))

(when (featurep! :editor evil +everywhere)
  (use-package! evil-collection
    :init
    (setq evil-collection-calendar-want-org-bindings t
          evil-collection-outline-bind-tab-p         t
          evil-collection-setup-minibuffer           t)
    :config
    (evil-collection-init)))

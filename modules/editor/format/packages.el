;;; editor/format/packages.el -*- no-byte-compile: t; -*-

(when (featurep! :editor format +apheleia)
  (package! apheleia
            :recipe (:host github
                     :repo "raxod502/apheleia")))

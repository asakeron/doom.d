;;; editor/format/config.el

(when (featurep! :editor format +apheleia)
  (use-package! apheleia
    :config
    (apheleia-global-mode 1)))

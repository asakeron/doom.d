;;; completion/selectrum/config.el

(use-package! selectrum
  :init
  (when (featurep! :completion selectrum +orderless)
    (setq selectrum-highlight-candidates-function #'orderless-highlight-matches
          orderless-skip-highlighting             (lambda () selectrum-is-active)))
  :config
  (when (featurep! :completion selectrum +savehist)
    (savehist-mode 1))
  (selectrum-mode 1))

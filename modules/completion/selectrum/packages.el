;;; completion/selectrum/packages.el -*- no-byte-compile: t; -*-

(package! selectrum)

(when (featurep! :completion selectrum +orderless)
  (package! orderless))

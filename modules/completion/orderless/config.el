;;; completion/orderless/config.el -*- lexical-binding: t; -*-

(use-package! orderless
  :init
  (setq completion-styles '(orderless)))

;;; tools/lsp/packages.el -*- lexical-binding: t; -*-

(when (featurep! :tools lsp +eglot)
  (package! eglot))

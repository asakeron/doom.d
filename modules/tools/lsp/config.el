;;; tools/lsp/config.el -*- lexical-binding: t; -*-

(when (featurep! :tools lsp +eglot)
  (use-package! eglot))
